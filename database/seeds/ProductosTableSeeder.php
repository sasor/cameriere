<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('productos')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $bebidas = [
            'Cappucino' => 'Capucino es una bebida refrescante dulce de cafe con leche , con una porcion de crema',
            'Espresso' => 'Un esspresso es una dimunuta taza de cafe concentrado',
            'Americano'  => 'Un americano es la mezcla de un espresso con una taza de agua caliente',
            'cola cola' => 'Una refrescante bebida fria'
        ];

        $platos = [
            'Chicken alla Diavola' =>'Es un plato tipico italiano que consiste en un delicioso pollo con especias',
            'Spaghetti' =>'Deliciosa combinacion de pasta con nuestra especialidad de salsa roja',
            'Ravioli' =>'Pasta rellena de queso o carne, cubierta con una salsa de tomate y queso parmesano',
            'Lasagne' => 'Especialidad de la casa,viene en capas de pasta carne, queso y salsa blanca',
            'Risotto' => 'Mezcla de arroz con queso y especias en combinacion, pregunte al mesero por elecciones'
        ];

        $postres = [
            'Brownie' => 'Brownie de chocolate caliente con una porcion de helado de vainilla y crema a eleccion',
            'Muffin' => 'Deliciosos postres de sabores, con chispas o moras, con una base vainilla',
            'Cheesecake' => 'Una crujiente base de galleta y adornado con una deliciosa mermelada de moras',
            'Canasta de Galletas' => 'Canaste de galletas de mantequilla con salsa de chocolate blanco'
        ];


        foreach ($bebidas as $key => $value) {
            DB::table('productos')->insert([
                'nombre' => $key,
                'categoria' => 'bebidas',
                'descripcion' => $value,
                'precio' => 10.20
            ]);
        }

        foreach ($platos as $key => $value) {
            DB::table('productos')->insert([
                'nombre' => $key,
                'categoria' => 'platos',
                'descripcion' => $value,
                'precio' => 13.20
            ]);
        }

        foreach ($postres as $key => $value) {
            DB::table('productos')->insert([
                'nombre' => $key,
                'categoria' => 'postres',
                'descripcion' => $value,
                'precio' => 7.80
            ]);
        }
    }
}
