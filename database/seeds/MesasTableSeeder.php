<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MesasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('mesas')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $tables = [true,true,true,true,true,true,true,true,true,true];

        foreach ($tables as $numeral => $table) {
            DB::table('mesas')->insert([
                'numeral' => ++$numeral,
                'estado' => $table
            ]);
            $this->command->info("Mesa con numero ${numeral} con estado disponible creado");
        }
    }
}
