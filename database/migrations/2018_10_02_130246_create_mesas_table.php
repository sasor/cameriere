<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mesas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('estado');
            $table->timestamps();
        });
        Schema::create('mesa_producto', function (Blueprint $table) {

            $table->integer('mesa_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->foreign('mesa_id')->references('id')->on('mesas')->onDelete('cascade');
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('cascade');
            $table->integer('cantidad')->unsigned();
            $table->text('comentario')->nullable();
            $table->boolean('cocina')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mesa_producto');
        Schema::dropIfExists('mesas');
    }
}
