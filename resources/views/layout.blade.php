<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <title>Cameriere</title>
    <style>
        .bordera {
            background-color: #000000;
        }
        .bordera2 {
            background-color: #616161;
        }
        .border1 {
            background-color: blue;
        }
        .border2 {
            background-color: violet;
        }
        .border3 {
            background-color: firebrick;
        }
        .border4 {
            background-color: palegreen;
        }
        .vh {
            height: 100vh;
        }
        .ri {
            min-height: 8vh;
        }
        .ro {
            min-height: 92vh;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row ri bordera">
        <div class="col-2 col-sm-2  col-md-2  col-lg-2">brand</div>
        <div class="col-10 col-sm-10 col-md-10 col-lg-10">header</div>
    </div>
    <div class="row ro">
        <div class="col-2  col-sm-2  col-md-2  col-lg-2 bordera2">aside</div>
        <div class="col-10 col-sm-10 col-md-10 col-lg-10">

            @if(session()->has('info'))
                <div class="container mt-4">
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-{{ session('type') ?? 'success' }} alert-dismissible fade show">
                                {{ session('info') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @yield('content')
        </div>
    </div>
</div>
<script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
@yield('scripts')
</body>
</html>
