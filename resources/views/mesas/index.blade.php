@extends('layout')

@section('content') 
<div class="container">
    <div class="row mt-3">
        <div class="col-12 col-md-10">
            <div class="row ">
               @foreach($mesas as $mesa)

                <div class="col-6 col-sm-6 col-md-4 col-lg-3">
                    <a href="{{ route('pedido.create', [ 'mesa' => $mesa->id]) }}">
                        <div class="card text-center text-black-50 bg-light mb-3 rounded" style="height: 10rem;">
                            <div class="card-body">
                                <div class="card-title">Mesa #{{ $mesa->numeral }}</div>
                                @if($mesa->estado)
                                    <div class="card-text">
                                        <span class="badge badge-success">Disponible</span>
                                    </div>
                                @else
                                    <div class="card-text">
                                        <span class="badge badge-danger">Ver Pedido</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </a>
                </div>

                @endforeach
            </div>
        </div>
        <div class="col-12 col-md-2">
            <h4>Modificar Cantidad Mesas</h4>

            <div class="d-flex">
                <a href="{{ route('mesas.increment') }}" class="p-2 flex-fill bg-dark text-center text-monospace text-white">
                    <i class="fas fa-plus"></i>
                </a>
                <div class="p-2 flex-fill bg-white text-center text-monospace">{{ count($mesas) }}</div>
                <a href="{{ route('mesas.decrement') }}" class="p-2 flex-fill bg-dark text-center text-monospace text-white">
                    <i class="fas fa-minus"></i>
                </a>
            </div>
        </div>
    </div>
</div>

@endsection
