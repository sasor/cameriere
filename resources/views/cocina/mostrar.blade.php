@extends('layout')

@section('content') 
<div class="container mt-5" style="min-height: 100%;">
    <div class="row">
        <div class="col-12 col-md-10">
            <table class="table table-striped">
                <thead>
                    <tr class="info">
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Mesa</th>
                        <th>Descripción</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($unidad as $mesaUnidad)
                    <tr class="info">
                        <td>{{ $mesaUnidad->nombre }}</td>
                        <td>{{ $mesaUnidad->cantidad }}</td>
                        <td>{{ $mesaUnidad->numeral }}</td>
                        <td>{{ $mesaUnidad->comentario }}</td>
                        <td>
                            <form method="POST" action="{{ route('pedidos.update',[$mesaUnidad->id, $mesaUnidad->producto_id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-success">Listo</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-12 col-md-2">
            <div class="d-flex justify-content-center">
                <a href="" class="btn btn-outline-warning my-2" onclick="location.reload()"><i class="fas fa-save"></i> Actualizar</a>
            </div>
        </div>
    </div>
</div>
@endsection
