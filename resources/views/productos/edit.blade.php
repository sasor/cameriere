@extends('layout')
@section('content')

<div class="container mt-5">
    <div class="row">
        <div class="col-12 col-md-6 offset-3">
            <div class="card">
                <h3 class="card-header">
                    Editar Producto
                </h3>
                <div class="card-body">
                    <form action="{{route('productos.update', [$producto->id]) }}"
                          method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Nombre del Producto</label>
                            <input id="name"
                                   class="form-control"
                                   type="text"
                                   name="nombre"
                                   pattern="[A-Za-z]* ?[A-Za-z]* ?[A-Za-z]* ?[A-Za-z]* ?"
                                   maxlength="100" required
                                   placeholder="Nombre Producto"
                                   value= "{{ $producto->nombre }}" >
                        </div>
                        <div class="form-group">
                            <label for="category">Categoría del Producto</label>
                            <select name="categoria" class="form-control" id="category" required>

                              <option selected disabled>Elija una categoría</option>
                              @if($producto ->categoria == 'bebidas') {
                                <option value="bebidas" id="bebidas" selected="">bebidas</option>
                              @elseif($producto ->categoria == 'platos')
                               <option value="platos" id="platos" selected="">platos</option>
                              @else
                                <option value="postres" id="postres" selected="">postres</option>
                              @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Descripción del Producto</label>
                            <textarea class="form-control"
                                      name="descripcion"
                                      id="description"
                                      placeholder="Descripción Producto"
                                      maxlength="200"
                                      rows="6" required>{{ $producto->descripcion }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="precio">Precio del Producto</label>
                            <input type="number"
                                   step=".01"
                                   min="1"
                                   class="form-control"
                                   name="precio" id="precio" placeholder="Precio Producto" required
                                   value={{ $producto->precio }} >
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-outline-warning">
                                <i class="fas fa-save"></i> Actualizar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
