<div class="col-12">
    <div class="card border-secondary mb-1 zoom">
        <div class="card-body text-secondary">
            <h5 class="card-title">{{ $producto->nombre }}</h5>
            <p class="card-text">{{ $producto->descripcion }}</p>
             <p class="card-text font-weight-bold">
                    Precio
                    <span class="badge badge-secondary">{{ $producto->precio }}</span>
            </p>
        </div>
        <div class="card-footer bg-transparent">
            <a href="{{action('ProductoController@edit',$producto->id)}}" class="btn btn-primary">
                <i class="fas fa-edit"></i> Editar
            </a>
            <form class="d-inline-flex" method="POST" action="{{ route('productos.destroy', [$producto->id])}}">
                @csrf
                @method('DELETE')
                <button type="button" class="btn btn-danger" data-toggle="modal"
                        data-target="#confirm_eliminar_modal">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
            </form>
        </div>
    </div>
</div>
