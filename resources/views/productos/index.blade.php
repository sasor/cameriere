@extends('layout')

@section('content')
    <style>
        .zoom {
            transition: transform .2s;
            margin: 0 auto;
        }

        .zoom:hover {
            transform: scale(0.97);
        }
    </style>
    <div class="container">
        <div class="row mt-3">
            <div class="col-12 col-md-10">
                @include('productos.productosLista')
            </div>
            <div class="col-12 col-md-2">
                <a class="btn btn-outline-warning"
                   href="{{action('ProductoController@create')}}">
                    <i class="fa fa-save"></i> Registrar Producto
                </a>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="confirm_eliminar_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmando Eliminar Producto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div>¿Está seguro de eliminar el producto?</div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="modal-btn-si">Confirmar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="modal-btn-no">Cerrar</button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            let tab = "{{ session('tab') ?? '' }}"
            if (tab) {
                $('#myTab a[href="#'+tab+'"]').tab('show')
            }
        })
    </script>
    <script>
        $('#confirm_eliminar_modal').on('shown.bs.modal', function (event) {
            $('#modal-btn-si').on('click', () => {
                event.relatedTarget.form.submit()
            });
        })

        $('#confirm_eliminar_modal').on('hide.bs.modal', function (event) {
            $('#modal-btn-si').off('click')
        })
    </script>
@endsection
