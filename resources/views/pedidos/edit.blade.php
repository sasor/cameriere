@extends('layout')

@section('content')
    <style>
        .zoom {
            transition: transform .2s;
            margin: 0 auto;
        }

        .zoom:hover {
            transform: scale(0.97);
        }
    </style>
    <div class="container d-none" id="info">
        <div class="col">
            <div class="alert alert-success" role="alert"></div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-3">
            <div class="col-12 col-md-10">
                @include('pedidos.productos')
            </div>
            <div class="col-12 col-md-2">
                <button type="button"
                        id="confirm_pedido"
                        class="btn btn-outline-warning w-100 my-2 disabled"
                        disabled>
                    <i class="fas fa-save"></i> Actualizar
                </button>
            </div>
            <div class="modal" tabindex="-1" role="dialog" id="confirm_pedido_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmando Pedido</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <ul class="list-group list-group-flush"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="order_confirmed" class="btn btn-primary">Confirmar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        const start_counter = (id_pedido, value) => {
            if (! cameriere.pedido.has(id_pedido)) {
                cameriere.pedido.set(id_pedido, value)
            }
        }

        const enable_button_save = () => {
            let btn = document.querySelector("#confirm_pedido")
            btn.disabled = false
            btn.classList.remove('disabled')
        }

        const enable_counter_ui = (id_producto) => {
            document.querySelector(`#card-producto-${id_producto} > .card-footer`).classList.remove('d-none')
            enable_button_save()
        }

        const prepare_pedido = (id_producto) => {
            start_counter(id_producto, 1)
            enable_counter_ui(id_producto)
        }

        const decrement_quantity = (id_producto) => {
            if (cameriere.pedido.get(id_producto) > 1) {
                cameriere.pedido.set(id_producto, (cameriere.pedido.get(id_producto) - 1))
                update_quantity_ui(id_producto, cameriere.pedido.get(id_producto))
            }
        }

        const increment_quantity = (id_producto) => {
            cameriere.pedido.set(id_producto, (cameriere.pedido.get(id_producto) + 1))
            update_quantity_ui(id_producto, cameriere.pedido.get(id_producto))
        }

        const update_quantity_ui = (id_producto, value) => {
            document.querySelector(`#toogle_${id_producto}`).innerHTML = value;
        }

        const get_productos_from_pedido = (pedido, productos) => {
            let output = []
            pedido.forEach((value, key, map) => {
                let producto_pedido = productos.find(producto => producto.id == key)
                producto_pedido.cantidad = value
                output.push(producto_pedido)
            })
            return output
        }

        const confirm_pedido = () => {

            let data = get_productos_from_pedido(cameriere.pedido, cameriere.menu)

            body = '';
            for (let item of data) {
                body += `
                <li class="list-group-item">
                        <div>
                            <div class="float-right">${item.nombre}</div>
                            <div class="float-left">
                                <span class="badge badge-secondary">${item.cantidad}</span>
                            </div>
                        </div>
                </li>
            `;
            }

            document.querySelector('#confirm_pedido_modal div.modal-body ul.list-group').innerHTML = body
            if (data.length != 0) {
                $('#confirm_pedido_modal').modal()
            }

        }

        const build_data = (pedido, comments) => {
            let data = [];
            pedido.forEach((value, key, map) => {
                comentario = comments.get(key) || ''
                data.push({ producto : key, cantidad : value, comentario: comentario})
            })
            return data
        }

        const send_orders_to_save = () => {

            cameriere.comentarios.clear();
            for (let id_producto of cameriere.pedido.keys()) {
                let comment = document.getElementById(`comentario_${id_producto}`)
                if (!(comment.value.trim() == '') ||
                    !(comment.value.trim().length == 0)) {
                    cameriere.comentarios.set(id_producto, comment.value)
                }
            }
            data = build_data(cameriere.pedido, cameriere.comentarios)

            axios({
                url: `/pedido/${cameriere.mesa.id}`,
                method: 'put',
                data: {
                    data: JSON.stringify(data)
                }
            }).then(function (res) {
                $('#confirm_pedido_modal').modal('hide')
                document.querySelector("#info div div").innerHTML = res.data.mensaje
                document.querySelector("#info").classList.remove('d-none');
                setTimeout(() => {
                    window.location.replace(`/pedido/${cameriere.mesa.id}/create`)
                }, 1000)
            }).catch(function (err) {
                console.log(err)
            });
        }

        const comentary_producto = (key) => {
            document.getElementById(`comentario_${key}`).value = cameriere.comentarios.get(key) || ''
        }

        const set_pedido = () => {
            cameriere.pedido.forEach((value, key, map) => {
                update_quantity_ui(key,value);
                comentary_producto(key);
                enable_counter_ui(key);
            })
        }

        const delete_producto = (e, id_producto) => {
            cameriere.pedido.delete(id_producto)
            update_quantity_ui(id_producto, 1)
            document.querySelector(`#card-producto-${id_producto} > .card-footer`).classList.add('d-none')
            e.stopPropagation()
        }

        document.addEventListener("DOMContentLoaded", () => {

            window.cameriere = {};

            cameriere.mesa = @json($mesa);
            cameriere.menu = Object.values(@json($productos)).flat();
            cameriere.pedido = new Map(
                cameriere.mesa.productos.map( producto => {
                    return Object.values({id: producto.id, cantidad: producto.pivot.cantidad})
                }
            ));
            cameriere.comentarios = new Map(
                cameriere.mesa.productos.map( producto => {
                    return Object.values({id: producto.id, comentario: producto.pivot.comentario})
                }
            ));

            document.querySelector('#confirm_pedido').addEventListener('click', confirm_pedido);
            document.querySelector('#order_confirmed').addEventListener('click', send_orders_to_save);
        })

        window.addEventListener('load', () => {
            set_pedido();
        })

    </script>
@endsection
