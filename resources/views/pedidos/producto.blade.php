<div class="col-12 col-md-6">
    <div class="card border-secondary mb-1 zoom"
         style="cursor: pointer;"
         onclick="prepare_pedido({{ $producto->id }})"
         id="card-producto-{{ $producto->id }}">
        <div class="card-body text-secondary">
            <div class="float-left">
                <h5 class="card-title">{{ $producto->nombre }}</h5>
                <p class="card-text">{{ $producto->descripcion }}</p>
                <p class="card-text font-weight-bold">
                    Precio
                    <span class="badge badge-secondary">{{ $producto->precio }}</span>
                </p>
            </div>
        </div>
        <div class="card-footer d-none">
            <div class="d-flex">
                <div class="p-2 flex-fill bg-dark text-center text-monospace text-white" onclick="increment_quantity({{ $producto->id }})"><i class="fas fa-plus"></i></div>
                <div class="p-2 flex-fill bg-white text-center text-monospace" id="toogle_{{ $producto->id }}">1</div>
                <div class="p-2 flex-fill bg-dark text-center text-monospace text-white" onclick="decrement_quantity({{ $producto->id }})"><i class="fas fa-minus"></i></div>
                <div class="p-2 flex-fill bg-secondary text-center text-monospace text-white" onclick="delete_producto(event, {{ $producto->id }})"><i class="fas fa-times"></i></div>
            </div>
            <div class="d-flex">
                <textarea class="form-control"
                          name="comentario_{{ $producto->id }}"
                          id="comentario_{{ $producto->id }}"
                          placeholder="Detalle"
                          maxlength="200"
                          rows="6"></textarea>
            </div>
        </div>
    </div>
</div>
