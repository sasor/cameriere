@extends('layout')

@section('content')
    <style>
        .zoom {
            transition: transform .2s;
            margin: 0 auto;
        }

        .zoom:hover {
            transform: scale(0.97);
        }
    </style>
    <div class="container d-none" id="info">
        <div class="col">
            <div class="alert alert-success" role="alert"></div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-3">
            <div class="col-12 col-md-10">
                <input type="hidden" value="{{ $mesa->id }}" id="mesa_id">
                @include('pedidos.productos')
            </div>
            <div class="col-12 col-md-2">
                <button type="button" id="confirm_pedido" class="btn btn-outline-warning w-100 my-2 disabled" disabled>
                    <i class="fas fa-save"></i> Añadir
                </button>
            </div>
            <div class="modal" tabindex="-1" role="dialog" id="confirm_pedido_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmando Pedido</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <ul class="list-group list-group-flush"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                            <button type="button" id="order_confirmed" class="btn btn-primary">Confirmar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>

        const prepare_pedido = (id_pedido) => {
            start_counter(id_pedido)
            enable_counter_ui(id_pedido)
        }

        const start_counter = (id_pedido) => {
            if (! window.pedido.has(id_pedido)) {
                window.pedido.set(id_pedido,1)
            }
        }

        const enable_button_save = () => {
            let btn = document.querySelector("#confirm_pedido")
            btn.disabled = false
            btn.classList.remove('disabled')
        }

        const enable_counter_ui = (id_pedido) => {
            document.querySelector(`#card-producto-${id_pedido} > .card-footer`).classList.remove('d-none')
            enable_button_save()
        }

        const update_quantity_ui = (id_producto, value) => {
            document.querySelector(`#toogle_${id_producto}`).innerHTML = value;
        }

        const decrement_quantity = (id_producto) => {
            if (window.pedido.get(id_producto) > 1) {
                window.pedido.set(id_producto, (window.pedido.get(id_producto) - 1))
                update_quantity_ui(id_producto, window.pedido.get(id_producto))
            }
        }

        const increment_quantity = (id_producto) => {
            window.pedido.set(id_producto, (window.pedido.get(id_producto) + 1))
            update_quantity_ui(id_producto, window.pedido.get(id_producto))
        }

        const get_productos_from_pedido = (pedido, productos) => {
            let output = []
            pedido.forEach((value, key, map) => {
                let producto_pedido = productos.find(producto => producto.id == key)
                producto_pedido.cantidad = value
                output.push(producto_pedido)
            })
            return output
        }

        const confirm_pedido = () => {
            let data = get_productos_from_pedido(window.pedido, window.productos)

            body = '';
            for (let item of data) {
                body += `
                <li class="list-group-item">
                        <div>
                            <div class="float-right">${item.nombre}</div>
                            <div class="float-left">
                                <span class="badge badge-secondary">${item.cantidad}</span>
                            </div>
                        </div>
                </li>
            `;
            }

            document.querySelector('#confirm_pedido_modal div.modal-body ul.list-group').innerHTML = body
            if (data.length != 0) {
                $('#confirm_pedido_modal').modal()
            }

        }

        const build_data = (pedido, comments) => {
            let data = [];
            pedido.forEach((value, key, map) => {
                comentario = comments.get(key) || ''
                data.push({ producto : key, cantidad : value, comentario: comentario})
            })
            return data;
        }

        const send_orders_to_save = () => {
            let mesa = document.querySelector("#mesa_id").value

            window.detail_pedido.clear();
            for (let id_producto of window.pedido.keys()) {
                let comment = document.getElementById(`comentario_${id_producto}`)
                if (!(comment.value.trim() == '') ||
                    !(comment.value.trim().length == 0)) {
                    window.detail_pedido.set(id_producto, comment.value)
                }
            }
            data = build_data(window.pedido, window.detail_pedido)

            axios({
                url: '/pedido',
                method: 'post',
                data: {
                    mesa: mesa,
                    data: JSON.stringify(data)
                }
            }).then(function (res) {
                $('#confirm_pedido_modal').modal('hide')
                document.querySelector("#info div div").innerHTML = res.data.mensaje
                document.querySelector("#info").classList.remove('d-none');
                setTimeout(() => {
                    window.location.replace(`/mesas`)
                }, 1000)
            }).catch(function (err) {
                console.log(err)
            });
        }

        const delete_producto = (e, id_producto) => {
            window.pedido.delete(id_producto)
            update_quantity_ui(id_producto, 1)
            document.querySelector(`#card-producto-${id_producto} > .card-footer`).classList.add('d-none')
            e.stopPropagation()
        }

        document.addEventListener("DOMContentLoaded", () => {

            window.pedido = new Map()
            window.detail_pedido = new Map()
            window.productos = Object.values(@json($productos)).flat()
            window.mesa = @json($mesa)


            document.querySelector('#confirm_pedido').addEventListener('click', confirm_pedido)
            document.querySelector('#order_confirmed').addEventListener('click', send_orders_to_save)
        })

    </script>
@endsection
