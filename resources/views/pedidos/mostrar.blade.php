@extends('layout')

@section('content')
    <div class="container">
        <div class="row mt-4">
            <div class="col">
                <h2>Pedido Mesa {{ $numeral }}</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr class="info">
                                <th>Plato</th>
                                <th>Cantidad</th>
                                <th>Precio Unidad</th>
                                <th>Precio</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($unidad as $mesaUnidad)
                                <tr class="info">
                                    <td>{{ $mesaUnidad->nombre }}</td>
                                    <td>{{ $mesaUnidad->cantidad }}</td>
                                    <td>{{ $mesaUnidad->precio }}</td>
                                    <td>{{ $mesaUnidad->totalUni }} Bs</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                                <th>{{ $total->total }} Bs</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row mt-5 d-flex justify-content-around">
            <a href="{{ route('pedido.edit',$id) }}" class="btn btn-primary">
                <i class="fas fa-edit"></i> Editar
            </a>
            <button type="button" id="confirm_pedido" class="btn btn-danger">
                <i class="fas fa-trash-alt"></i> Cerrar
            </button>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="confirm_pedido_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cerrar Pedido</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div>El total de la cuenta es: </div>
                                <div>{{ $total->total}} Bs</div>
                                <div>El cliente pago la cuenta en caja</div>
                                <div>¿Está seguro de cerrar el pedido ?</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                     <button type="button" id="order_confirmed" class="btn btn-primary">
                        <a style="color: white;" href="{{ action('MesaController@update', $id) }}"> Confirmar</a>
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                   
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        const confirm_pedido = () => {
            $('#confirm_pedido_modal').modal();
        }
        const hide_modal = () => {
            $('#confirm_pedido_modal').modal('hide');
        }

        document.addEventListener("DOMContentLoaded", () => {
            document.querySelector('#confirm_pedido').addEventListener('click', confirm_pedido);
            document.querySelector('#order_confirmed').addEventListener('click', hide_modal);
        })
    </script>
@endsection

