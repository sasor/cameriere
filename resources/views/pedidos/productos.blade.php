<ul class="nav nav-tabs d-flex justify-content-center justify-content-md-around" id="myTab" role="tablist">
    <li class="nav-item w-25 text-center">
        <a class="nav-link active bg-dark text-white" id="bebidas-tab" data-toggle="tab" href="#bebidas" role="tab" aria-controls="bebidas" aria-selected="true">Bebidas</a>
    </li>
    <li class="nav-item w-25 text-center">
        <a class="nav-link bg-dark text-white" id="platos-tab" data-toggle="tab" href="#platos" role="tab" aria-controls="platos" aria-selected="false">Platos</a>
    </li>
    <li class="nav-item w-25 text-center">
        <a class="nav-link bg-dark text-white" id="postres-tab" data-toggle="tab" href="#postres" role="tab" aria-controls="postresx" aria-selected="false">Postres</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="bebidas" role="tabpanel" aria-labelledby="bebidas-tab">
        <div class="row">
            @if(!empty($productos['bebidas']))
                @foreach($productos['bebidas'] as $producto)
                    @include('pedidos.producto')
                @endforeach
            @else
                <div class="col">
                    <div class="alert alert-info" role="alert" id="alert">
                        <label>No existen productos en esta categoría.</label>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="tab-pane fade" id="platos" role="tabpanel" aria-labelledby="platos-tab">
        <div class="row">
            @if(!empty($productos['platos'])) 
                @foreach($productos['platos'] as $producto)
                    @include('pedidos.producto')
                @endforeach
            @else
                <div class="col">
                    <div class="alert alert-info" role="alert" id="alert">
                        <label>No existen productos en esta categoría.</label>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="tab-pane fade" id="postres" role="tabpanel" aria-labelledby="postres-tab">
        <div class="row">
            @if(!empty($productos['postres'])) 
                @foreach($productos['postres'] as $producto)
                    @include('pedidos.producto')
                @endforeach
            @else
                <div class="col">
                    <div class="alert alert-info" role="alert" id="alert">
                        <label>No existen productos en esta categoría.</label>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
