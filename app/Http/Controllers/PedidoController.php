<?php

namespace App\Http\Controllers;

use App\Mesa;
use App\Producto;
use Illuminate\Http\Request;

class PedidoController extends Controller
{
    public function create(Mesa $mesa)
    {
        $products = Producto::orderBy('id')->get();
        $productos_categorizados = $products->groupBy('categoria');
        $idMesa = $mesa->estado;
        $id = $mesa->id;
        if ($mesa->estado) {
            return view('pedidos.create')->with('productos', $productos_categorizados)->with('mesa', $mesa);
        } else {
            $totalUnidad = \DB::table('mesas')
                ->join('mesa_producto', 'mesas.id', '=', 'mesa_producto.mesa_id')
                ->join('productos', 'productos.id', '=', 'mesa_producto.producto_id')
                ->select(
                    'productos.nombre',
                    'productos.precio',
                    'mesa_producto.cantidad',
                    (\DB::raw('(productos.precio*mesa_producto.cantidad) AS totalUni'))
                )
                ->where('mesas.id', '=', $id)
                ->get();

            $total = \DB::table('mesas')
                ->join('mesa_producto', 'mesas.id', '=', 'mesa_producto.mesa_id')
                ->join('productos', 'productos.id', '=', 'mesa_producto.producto_id')
                ->select(\DB::raw('sum(productos.precio*mesa_producto.cantidad) AS total'))
                ->where('mesas.id', '=', $id)
                ->get()->first();
            return view('pedidos.mostrar')
                ->with('unidad', $totalUnidad)
                ->with('total', $total)
                ->with('id', $id)
                ->with('numeral', $mesa->numeral);
        }
    }

    public function editar(Mesa $mesa)
    {
        $products = Producto::orderBy('id')->get();
        $productos_categorizados = $products->groupBy('categoria');
        $idMesa = $mesa->estado;
        $id = $mesa->id;
        
        return view('pedidos.create')->with('productos', $productos_categorizados)->with('mesa', $mesa);
    }

    public function store(Request $request)
    {
        $mesa = Mesa::find($request->mesa);
        $response = 'Error no puedes crear Pedido';

        if ($mesa->estado) {
            $orders = json_decode($request->data, true);
            foreach ($orders as $order) {
                $mesa->productos()->attach(
                    $order['producto'],
                    [
                    'cantidad' => $order['cantidad'],
                    'comentario' => $order['comentario']
                    ]
                );
            }
            $mesa->estado = false;
            $mesa->save();
            $response = 'Pedido agregado exitosamente.';
        }

        return response()->json(
            [
            'mensaje' => $response
            ]
        );
    }

    public function edit(Mesa $mesa)
    {
        $mesa->load('productos:id,nombre');
        $products = Producto::orderBy('id')->get();
        $productos_categorizados = $products->groupBy('categoria');

        return view('pedidos.edit')->with('productos', $productos_categorizados)
            ->with('mesa', $mesa);
    }

    public function update(Request $request, Mesa $mesa)
    {
        $orders = json_decode($request->data, true);

        $orders_to_save = [];
        foreach ($orders as $order) {
            $extras_columns = [
                'cantidad' => $order['cantidad'],
                'comentario' => $order['comentario']
            ];
            $orders_to_save[$order['producto']] = $extras_columns;
        }

        $mesa->productos()->sync($orders_to_save);

        return response()->json(
            [
            'mensaje' => 'Pedido actualizado exitosamente.'
            ]
        );
    }
}
