<?php

namespace App\Http\Controllers;

use App\Mesa;
use Illuminate\Http\Request;

class MesaController extends Controller
{
    public function index()
    {
        $mesas = Mesa::orderBy('id')->get();
        return view('mesas/index', compact('mesas'));
    }

    public function update(Request $request, $id)
    {

        $mesa = Mesa::find($id);
        $mesa->productos()->detach();
        if (!($mesa->estado)) {
            $mesa -> estado = true;
        }
        $mesa->save();

        $mesas = Mesa::orderBy('id')->get();
        return view('mesas/index', compact('mesas'));
    }

    public function increment()
    {
        $mesa = new Mesa();

        if ($mesa->currentNumeral() < Mesa::NUMERAL_MAX) {
            $mesa->numeral = $mesa->currentNumeral() + 1;
            $mesa->estado = true;
            $mesa->save();
            return redirect()->back();
        } else {
            return redirect()->back()
                ->with('info', 'Llegó al máximo de mesas.')
                ->with('type', 'info');
        }
    }

    public function decrement()
    {
        $mesas = Mesa::orderBy('id', 'desc')->get();
        if ($mesas->where('estado', 1)->isNotEmpty() && $mesas->count() > Mesa::NUMERAL_MIN) {
            $mesas->firstWhere('estado', 1)->delete();
            $this->reNumeral();
        }
        return redirect()->back();
    }

    private function reNumeral()
    {
        $mesas = Mesa::all();
        foreach ($mesas as $index => $mesa) {
            $newIndex = ++$index;
            if ($newIndex != $mesa->numeral) {
                $mesa->numeral = $newIndex;
                $mesa->save();
            }
        }
    }
}
