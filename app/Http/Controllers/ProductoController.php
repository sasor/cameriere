<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    public function index()
    {
        $products = Producto::orderBy('nombre')->get();
        $productos_categorizados = $products->groupBy('categoria');
        return view('productos.index')->with('productos', $productos_categorizados);
    }

    public function create()
    {
        return view('productos.create');
    }

    public function store(Request $request)
    {
        $producto = new Producto($request->all());
        $producto->save();

        return redirect()->route('productos.index')
            ->with("info", "Su producto se registró exitosamente.")
            ->with("tab", $producto->categoria);
    }

    public function destroy(Producto $producto)
    {
        $tab = $producto->categoria;
        $producto->delete();
        return redirect()->route('productos.index')->with('tab', $tab);
    }

    public function edit($id)
    {
        $producto = Producto::find($id);
        return view('productos.edit')->with('producto', $producto);
    }

    public function update(Request $request, Producto $producto)
    {
        $producto->fill($request->all());
        if ($producto->isDirty()) {
            $producto->save();
            return redirect()->route('productos.index')
                ->with("info", "Su producto se actualizó exitosamente.")
                ->with("tab", $producto->categoria);
        }
        return redirect()->route('productos.index')->with("tab", $producto->categoria);
    }
}
