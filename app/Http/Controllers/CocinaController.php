<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CocinaController extends Controller
{
    public function index()
    {
        $totalUnidad = \DB::table('mesas')
            ->join('mesa_producto', 'mesas.id', '=', 'mesa_producto.mesa_id')
            ->join('productos', 'productos.id', '=', 'mesa_producto.producto_id')
            ->select(
                'productos.nombre',
                'mesa_producto.cantidad',
                'mesas.id',
                'mesas.numeral',
                'mesa_producto.comentario',
                'mesa_producto.producto_id'
            )
            ->where('mesa_producto.cocina', '=', true)
            ->get();
  
        return view('cocina.mostrar')->with('unidad', $totalUnidad);
    }

    public function update(Request $request, $idMesa, $idProducto)
    {
        \DB::table('mesa_producto')
            ->where('mesa_id', $idMesa)
            ->where('producto_id', $idProducto)
            ->update(['cocina' => false]);
        
        return redirect()->route('pedidos.index');
    }
}
