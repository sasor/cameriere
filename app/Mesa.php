<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesa extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['estado'];

    const NUMERAL_MAX = 17;
    const NUMERAL_MIN = 1;

    public function productos()
    {
        return $this->belongsToMany('App\Producto')->withPivot('cantidad', 'comentario', 'cocina');
    }

    public function currentNumeral()
    {
        return Mesa::latest('id')->pluck('numeral')->first();
    }
}
