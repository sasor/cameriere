<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['nombre','categoria','descripcion','precio'];

    public function mesas()
    {
        return $this->belongsToMany('App\Mesa');
    }
}
