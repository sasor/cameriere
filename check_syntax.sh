#!/usr/bin/env bash

dirs=("app/*.php" "app/Http/Controllers/*.php" "routes/web.php")
read -p "Revisar sintaxis presione [c], arreglar sintaxis presione [f]: " option
case ${option:0:1} in
    c|C )
        for t in ${dirs[@]}; do
            php phpcs.phar --standard=PSR2 ${t}
        done
    ;;
    f|F )
        for t in ${dirs[@]}; do
            php phpcbf.phar --standard=PSR2 ${t}
        done
    ;;
    * )
        echo Adios.
    ;;
esac

#dirs=("app/*.php" "app/Http/Controllers/*.php" "routes/web.php")
#for t in ${dirs[@]}; do
#  php phpcs.phar --standard=PSR2 ${t}
#done
