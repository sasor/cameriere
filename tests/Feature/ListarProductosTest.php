<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ListarProductosTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
 
	public function testExisteVista()
	 {
	 	$response = $this->call('GET', 'menu/productos');
	 	$response->assertStatus(200);
	 }

	public function testMuestrarBebidas()
    {
        $response = $this->call('GET', 'menu/productos');
        $view = $response->getContent();
        $this->assertContains('Bebidas', $view);
    }

	public function testMuestraPostres()
    {
        $response = $this->call('GET', 'menu/productos');
        $view = $response->getContent();
        $this->assertContains('Postres',$view);
    }

	public function testMuestraPlatos()
    {
        $response = $this->call('GET', 'menu/productos');
        $view = $response->getContent();
        $this->assertContains('Platos',$view);
    }

    public function testBotonRegistrar()
    {
        $response = $this->call('GET', 'menu/productos');
        $view = $response->getContent();
        $this->assertContains('Registrar Producto',$view);
    }

    public function testRedirigeRuta(){
    	$this->visit('/menu/productos/')
 			 ->press('Registrar Producto')
 			 ->assertPathIs('/menu/productos/create');
    }

}
