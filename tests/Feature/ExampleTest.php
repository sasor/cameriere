<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    function test_should_load_mesas()
    {
        $this->get('/mesas')
            ->assertStatus(200);
    }

    /** @test */
    function test_should_load_pedido()
    {
        $mesa = 1;
        $this->get("/pedido/{$mesa}/create")
            ->assertStatus(200);
    }

    /** @test */
    function test_should_fail_pedido_with_inexistent_mesa()
    {
        $mesa = 100;
        $this->get("/pedido/{$mesa}/create")
            ->assertStatus(404);
    }
}
