<?php

Route::get(
    '/',
    function () {
        return view('layout');
    }
);

Route::get('/mesas', 'MesaController@index')->name('mesas.index');
Route::get('/mesas/increment', 'MesaController@increment')->name('mesas.increment');
Route::get('/mesas/decrement', 'MesaController@decrement')->name('mesas.decrement');
Route::get('/pedido/{mesa}/create', 'PedidoController@create')->name('pedido.create');
Route::get('/mesas/{id}', 'MesaController@update')->name('mesas.index');
Route::get('/pedido/{mesa}/editar', 'PedidoController@editar')->name('pedido.edit');
Route::post('/pedido', 'PedidoController@save')->name('pedido.save');
Route::get('/pedido/{mesa}/edit', 'PedidoController@edit')->name('pedido.edit');
Route::put('/pedido/{mesa}', 'PedidoController@update')->name('pedido.update');
Route::post('/pedido', 'PedidoController@store')->name('pedido.store');

Route::group(
    ['prefix'=>'menu'],
    function () {
        Route::resource('productos', 'ProductoController');
    }
);

Route::group(
    ['prefix'=>'cocina'],
    function () {
        Route::get('pedidos', 'CocinaController@index')->name('pedidos.index');
        Route::post('pedidos/{idMesa}/{idProducto}', 'CocinaController@update')->name('pedidos.update');
    }
);
